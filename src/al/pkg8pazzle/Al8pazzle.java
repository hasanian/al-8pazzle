package al.pkg8pazzle;

import al.pkg8pazzle.*;
import al.pkg8pazzle.Util.Board;
import al.pkg8pazzle.Util.Node;
import java.io.Console;
import java.util.ArrayList;
import java.util.List;

public class Al8pazzle {
    
    public static void main(String[] args) {
        Util util = new Util();
        Node currentNode = (util).new Node();
        
        Board target = (util).new Board();//target board (the pazzle will be solved when board of currentNode and target be equal)
        //inital target board
        target.fields[0] = "B";
        for (int i = 1; i < target.fields.length; i++) {
            target.fields[i] = i + "";
        }
        
        
        currentNode.parant = null;//this the root node 
        currentNode.board = util.generateRandomBoard();//asign random fields to board
        
        //test
//        currentNode.board.fields[2] = "B";
//        for(int i =3 ; i<9 ; i++)
//        {
//            currentNode.board.fields[i] = i+"";
//        }
//        currentNode.board.fields[1] = 2+"";
//        currentNode.board.fields[0] = 1+"";
        
        //test

        util.printBoard(currentNode.board);//output -> initial step
        
        List<Board> visitedBoards = new ArrayList<>();
        visitedBoards.add(currentNode.board);
        
        while(!util.checkBoardEquality(currentNode.board, target))
        {
            List<Board> childs = util.nextPosibleBoard(currentNode.board);
            
            //add unvisited board to child of currentNode
            for(Board child : childs)//for each child checks that is visited previously
            {
                boolean found = false;
                for(Board visitedBoard : visitedBoards)//checks that is special child in list of visitedBoards
                {
                    if(util.checkBoardEquality(child, visitedBoard))
                    {
                        found = true;
                        break;
                    }
                }
                if(!found)
                {
                    //make child node for add to currentNode
                    Node temp = util.new Node();
                    temp.parant = currentNode;
                    temp.board = child;
                    currentNode.childs.add(temp);
                }
            }
            
            //find low cast child 
            //assign it to currentNode
            //add currentNode to visited node
            //
            //
            
            if(currentNode.childs.size() == 0)//if there is not child to choose from so pazzle is not solvable
            {
                System.out.println("pazzle is not solvable");
                return;
            }
            
            
            int minH = util.h(currentNode.childs.get(0).board, target);
            Node minHNode = currentNode.childs.get(0);
            
            for(Node childNode : currentNode.childs)
            {
                int h = util.h(childNode.board, target);
                if(h < minH)
                {
                    minH = h;
                    minHNode = childNode;
                }
            }
            
            currentNode = minHNode;
              
            visitedBoards.add(currentNode.board);
            util.printBoard(currentNode.board);
            
        }
    }
    
}
