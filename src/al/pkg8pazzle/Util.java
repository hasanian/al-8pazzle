package al.pkg8pazzle;


import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import jdk.nashorn.internal.objects.NativeArray;

public class Util {
    
    public Util(){//ctor
    }
    
    public class Board{
        String fields[] = new String[9];
    }

    public void printBoard(Board board)
    {
        for(int i=0;i < board.fields.length;i++)
        {
            System.out.print(board.fields[i]);
            if( (i+1) %3 == 0)
                System.out.print("\n");
        }
        System.out.print("\n");
    }
    
    //generate random board for make initial state of board
    public Board generateRandomBoard()
    {
        List<String> field = new ArrayList<>();
        field.add("B");
        field.add("1");
        field.add("2");
        field.add("3");
        field.add("4");
        field.add("5");
        field.add("6");
        field.add("7");
        field.add("8");

        Board board = new Board();
        Random rand = new Random();
        
        for(int i=0;i<9;i++)
        {
            int randNumber = rand.nextInt(field.size());
            board.fields[i] = field.get(randNumber);
            field.remove(randNumber);
        }
        
        return board;
        
    }
    
    //handles some things about position
    public class Position{
        public int x;
        public int y;
        
        public Position(int x,int y)//ctor
        {
            this.x = x;
            this.y = y;
        }
        
        public Position(int lienarPos)//ctor
        {
            this.x = getXyPos(lienarPos).x;
            this.y = getXyPos(lienarPos).y;
        }
        
        //convert x and y position to linear position
        public int getLinearPos(int x,int y)
        {
            return (3*y + x);
        }
        
        public int getLinearPos()
        {
            return (3*this.y + this.x);
        }
        
        //convert liniear position to x and y position
        public Position getXyPos(int linearPos)
        {
            Position position = new Position(linearPos%3 , linearPos/3);
            
            return position;
        }
    }
    
    //swaps two fields (for move B in board)
    public Board swapBoardFields(Board board , int item1 , int item2)
    {
        String temp = new String();
        Board boardClone = new Board();
        boardClone.fields = board.fields.clone();
        temp = boardClone.fields[item1];
        boardClone.fields[item1] = boardClone.fields[item2];
        boardClone.fields[item2] = temp;
        
        return boardClone;
    }
    
    //generates next posible states according to current state
    public List<Board> nextPosibleBoard(Board board)
    {
        int bIndex = 0;
        List<Board> posibleBoards = new ArrayList<>();
        
        for (bIndex = 0; bIndex < board.fields.length ; bIndex++) {
            if(board.fields[bIndex].equals("B"))
            {
                break;
            }
        }
        
        Position bPosition = new Position(bIndex);
        List<Integer> newBPos = new ArrayList<Integer>();
        
        //adds posible movments to newBPos
        if(bPosition.x > 0)//shift B to left
        {
            newBPos.add((new Position(bPosition.x - 1 , bPosition.y)).getLinearPos());
        }
        
        if(bPosition.x < 2)//shift B to right
        {
            newBPos.add((new Position(bPosition.x + 1 , bPosition.y)).getLinearPos());
        }
        
        if(bPosition.y > 0)//shift B to bottom
        {
            newBPos.add((new Position(bPosition.x , bPosition.y-1)).getLinearPos());
        }
        
        if(bPosition.y < 2)//shift B to top
        {
            newBPos.add((new Position(bPosition.x , bPosition.y +1)).getLinearPos());
        }
        
        //make and add posible boards to posibleBoards
        for(int pos : newBPos)
        {
            posibleBoards.add(swapBoardFields(board, bIndex, pos));
        }
        
        return posibleBoards;
    }
    
    public int h(Board board, Board target)//guess cost
    {
        int cast = 0;
        for(int i=0;i<board.fields.length;i++)//calculate cost for each board field
        {
            int indexInTarget = 0;
            for(;indexInTarget < target.fields.length;indexInTarget++)//find index of board field in target
            {
                if(board.fields[i].equals(target.fields[indexInTarget]))
                    break;
            }
            
            
            Position sourcePos = new Position(i);
            Position targetPos = new Position(indexInTarget);
            
            cast += Math.abs(sourcePos.x - targetPos.x) + Math.abs(sourcePos.y - targetPos.y);
            
        }
        return cast;
    }
    
    public boolean checkBoardEquality(Board board , Board target)
    {
        boolean is_equal = true;
        for(int i =0 ; i< board.fields.length ; i++)
        {
            if(!board.fields[i].equals(target.fields[i]))
                return false;
        }
        return true;
    }
    
    public class Node{    
        public Node parant;
        public List<Node> childs;
        public Board board;

        public Node() {
            childs = new ArrayList<>();
            board = new Board();
        }
    }
}
